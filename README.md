## TuuperCove (New Version)

Built with [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Module
- [Redux](https://redux.js.org)
- [React](https://react.dev)
- [React-hot-toast](https://react-hot-toast.com)
- [Next.js](https://nextjs.org)
- [NextUI](https://nextui.org)
- [Framer Motion](https://framer.com/motion)
- [GSAP](https://gsap.com)

## Support me!
- [EasyDonate](https://ezdn.app/itzmenope)
- [GitHub Sponsors](https://github.com/sponsors/CcNopebruh)
Thanks for supporting!